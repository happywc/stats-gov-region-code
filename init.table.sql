-- table design
CREATE TABLE IF NOT EXISTS `region-code`(
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `code` VARCHAR(12) DEFAULT '' COMMENT '区域代码，根据parentCode，分别表示：省份、市级、县/区、镇/乡',
  `parentCode` VARCHAR(12) DEFAULT '0' COMMENT '父级区域代码, 0表示省级/直辖市代码；',
  `name` VARCHAR(50)  DEFAULT '' COMMENT '区域名称',
  `addDate` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '添加日期',
  PRIMARY KEY (`id`),
  KEY `code`(`code`),
  KEY `parentCode`(`parentCode`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COMMENT '行政区域数据';