#### 介绍

主要抓取 [国家统计局2018年统计的区划代码和城乡划分代码](http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2018/index.html)


#### 简单说明

1. init.table.sql表结构
2. init.data.sql导出的SQL数据文件
3. regionCode.py主要抓取脚本文件, 结合数据库支持断点续传抓取
4. mysql.py主要操作数据库脚本文件
